import socket
import sys
import cv2
import pickle
import numpy as np
import struct ## new
import zlib
import socket
import serial
import serial.tools.list_ports
from time import sleep
from tkinter import *
from tkinter import ttk
from tkinter import font
from functools import partial
import json
import requests
ips = {}
maxMice = 0
HOST = ''
PORT = 8485
port = 3000
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print('Socket created')

s.bind((HOST,PORT))
print('Socket bind complete')
s.listen(10)

def turn_led_on(mouse_num):
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ip = board_ip[mouse_num].get()
    ip = ip[12:]
    client.connect((ip, port))
    
    client.send("GET /27/on\n\n".encode()) #LED on command
    from_server = client.recv(4096)
    
    client.close()
    
    print(from_server.decode())

def turn_led_off(mouse_num):
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ip = board_ip[mouse_num].get()
    ip = ip[12:]
    client.connect((ip, port))
    
    client.send("GET /27/off\n\n".encode()) #LED off command
    from_server = client.recv(4096)

    client.close()
    
    print(from_server.decode())
    
# Called by the Refresh battery percentages button
def read_battery():
    for i in range(1, total_mice.get() + 1):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ip = board_ip[i].get()
        ip = ip[12:]
        client.connect((ip, port))
        client.send("GET /battery-level\n\n".encode()) #Get battery command
        from_server = client.recv(4096)
        client.close()
        print(from_server.decode())
        battery[i].set("Battery: " + from_server.decode() + "%")
    
# Called by the Get IP address button
def get_ip(mouse_num):
    ser_ports = serial.tools.list_ports.comports() #scan all USB connected ports
    detected_ESPs = []
    for p in ser_ports:
        if "Silicon Labs" in p.manufacturer: #check for possible ESP32s
            detected_ESPs += [p]
    
    if len(detected_ESPs) > 0:
        selected = detected_ESPs[0] #select the first ESP32 found
        ser = serial.Serial(selected.device, 115200, timeout=3) #connect to ESP32, baudrate=115200

        if ser.is_open:
            print("Connected to serial")
            print("Waiting 5 seconds... Application may temporarily freeze.")
            sleep(5) #sleep for 5 seconds to wait for serial connection
            ser.reset_input_buffer()
            ser.write(b"Get IP") #command to get IP address
            line = ser.readline()
            #print(line)
            ip_address = line[0:-2].decode() #cut off \r\n characters at end and convert to string
            print(ip_address)
            ips[mouse_num] = ip_address
            ser.close()
            
            board_ip[mouse_num].set("IP address: " + ip_address)
            
            #enable connection test buttons after IP obtained
            led_test_on[mouse_num].config(state="normal")
            led_test_off[mouse_num].config(state="normal")
            ready_box[mouse_num].config(state="normal")
            mouse_wifi_button[mouse_num].config(state="disabled")
            mouse_getip_button[mouse_num].config(state="disabled")
        else:
            print("Could not connect to ESP32.")
        #ser.close() #close serial port
    else:
        print("No ESP32s detected.")

# Called by the Reset IP address button
def reset_ip(mouse_num):
    ip = board_ip[mouse_num].get()
    if ip != "IP address: Not initialized":
        turn_led_off(mouse_num)
        
    if wifi_confirmed.get():
        mouse_wifi_button[mouse_num].config(state="normal")
        
    board_ip[mouse_num].set("IP address: Not initialized")
    led_test_on[mouse_num].config(state="disabled")
    led_test_off[mouse_num].config(state="disabled")
    mouse_getip_button[mouse_num].config(state="normal")
    led_test_var[mouse_num].set("OFF")
    mouse_ready[mouse_num].set(0)
    battery[mouse_num].set("Battery: " + "Not initialized")
    
    start_button.config(state="disabled")
    read_battery_button.config(state="disabled")
    
# Called when the Enter password checkbox is clicked
def password_enabler():
    if password_enable.get() == "0":
        password_field.config(state="disabled")
        wifi_password.set("")
    elif password_enable.get() == "1":
        password_field.config(state="normal")
        password_field.focus()
        
# Called by the Confirm button in the Wi-Fi settings frame
def wifi_confirmer():
    network_field.config(state="disabled")
    password_field.config(state="disabled")
    wifi_confirm_button.config(state="disabled")
    wifi_edit_button.config(state="normal")
    password_enable_box.config(state="disabled")
    wifi_confirmed.set(True)

    for i in range(1, 5):
        ip = board_ip[i].get()
        if ip == "IP address: Not initialized":
            mouse_wifi_button[i].config(state="normal")
    
# Called by the Edit button in the Wi-Fi settings frame
def wifi_editor():
    network_field.config(state="normal")
    password_enabler()
    wifi_confirm_button.config(state="normal")
    wifi_edit_button.config(state="disabled")
    password_enable_box.config(state="normal")
    network_field.focus()
    wifi_confirmed.set(False)
    
    for i in range(1, 5):
        mouse_wifi_button[i].config(state="disabled")
    
# Called by the Connect to Wi-Fi button
def wifi_connector(mouse_num):
    ser_ports = serial.tools.list_ports.comports() #scan all USB connected ports
    detected_ESPs = []
    for p in ser_ports:
        if "Silicon Labs" in p.manufacturer: #check for possible ESP32s
            detected_ESPs += [p]
    
    if len(detected_ESPs) > 0:
        selected = detected_ESPs[0] #select the first ESP32 found
        ser = serial.Serial(selected.device, 115200, timeout=3) #connect to ESP32, baudrate=115200

        if ser.is_open:
            print("Connected to serial")
            print("Waiting 5 seconds... Application may freeze temporarily.")
            sleep(5) #sleep for 5 seconds to wait for possible Wi-Fi timeout
            ser.reset_input_buffer() #clear serial of bootloader stuff
            ser.write(bytes("Set network name:" + wifi_network.get(), encoding='utf-8')) #command to get IP address
            line = ser.readline()
            print(line.decode())
            
            if password_enable.get() == "0":
                wifi_password.set("_NO___PASSWORD_") #Serial command to use right Wi-Fi method if no password
            ser.write(bytes("Set password:" + wifi_password.get(), encoding='utf-8')) #command to get IP address
            line = ser.readline()
            print(line.decode())
            if password_enable.get() == "0":
                wifi_password.set("")
            
            ser.write(b"WiFi Connect")
            line = ser.readline()
            print(line.decode())
            print("Waiting 5 seconds...  Application may freeze temporarily.")
            #sleep(3)
            root.after(5000) #wait 5 seconds for connection to be made
            line = ser.readline()
            print(line.decode())
            
            ser.write(b"Get IP")
            line = ser.readline()
            ip_address = line[0:-2].decode() #cut off \r\n characters at end and convert to string
            print(ip_address)
            ser.close()
            ips[mouse_num-1] = ip_address
            print("Serial done")
                
            board_ip[mouse_num].set("IP address: " + ip_address)
            led_test_on[mouse_num].config(state="normal")
            led_test_off[mouse_num].config(state="normal")
            ready_box[mouse_num].config(state="normal")
            mouse_wifi_button[mouse_num].config(state="disabled")
            mouse_getip_button[mouse_num].config(state="disabled")
        else:
            print("Could not connect to ESP32.")
        #ser.close() #close serial port
    else:
        print("No ESP32s detected.")
        
# Called by the uppermost Confirm button
def total_mice_confirmer():
    total_mice_confirm.config(state="disabled")
    total_mice_menu.config(state="disabled")
    total_mice_edit.config(state="normal")
    
    #Remove all frames (to put back later)
    wifi_frame.grid_remove()
    for i in range(1, 5):
        mouse_frame[i].grid_remove()
    
    #Reset all 'Ready' checkboxes
        mouse_ready[i].set(0)
    
    wifi_frame.grid()
    start_button.grid()
    read_battery_button.grid()
    ips['maxMice'] = total_mice.get()
    print(maxMice)
    if total_mice.get() >= 1:
        mouse1_frame.grid()
        title_frame.grid_configure(columnspan=2)
        wifi_frame.grid_configure(rowspan=1)
    if total_mice.get() >= 2:
        mouse2_frame.grid()
        title_frame.grid_configure(columnspan=3)
    if total_mice.get() >= 3:
        mouse3_frame.grid()
        wifi_frame.grid_configure(rowspan=2)
        #title_frame.grid_configure(columnspan=3)
    if total_mice.get() >= 4:
        mouse4_frame.grid()
        #title_frame.grid_configure(columnspan=3)
    
# Called by the uppermost Edit button
def total_mice_editor():
    total_mice_confirm.config(state="normal")
    total_mice_menu.config(state="normal")
    total_mice_edit.config(state="disabled")
    start_button.config(state="disabled")
    read_battery_button.config(state="disabled")
    
# Called when a Ready checkbox is clicked
def ready_check():
    total_ready = mouse_ready_1.get() + mouse_ready_2.get() + mouse_ready_3.get() + mouse_ready_4.get()
    if total_ready >= total_mice.get():
        start_button.config(state="normal")
        read_battery_button.config(state="normal")
    else:
        start_button.config(state="disabled")
        read_battery_button.config(state="disabled")


  
def recognition_code():
    print("Starting program...")
    url = 'http://localhost:8000'
    print(ips)
    myobj = json.dumps(ips)
    start_button.config(state="disabled")
    req = requests.post(url, data = myobj)

    print('res is ',req.text)
    conn,addr=s.accept()
    data = b""

    payload_size = struct.calcsize(">L")
    # print("payload_size: {}".format(payload_size))
    while True:
        while len(data) < payload_size:
            # print("Recv: {}".format(len(data)))
            data += conn.recv(4096)

        # print("Done Recv: {}".format(len(data)))
        packed_msg_size = data[:payload_size]
        data = data[payload_size:]
        msg_size = struct.unpack(">L", packed_msg_size)[0]
        # print("msg_size: {}".format(msg_size))
        while len(data) < msg_size:
            data += conn.recv(4096)
        frame_data = data[:msg_size]
        data = data[msg_size:]

        frame=pickle.loads(frame_data, fix_imports=True, encoding="bytes")
        frame = cv2.imdecode(frame, cv2.IMREAD_COLOR)
        cv2.imshow('ImageWindow',frame)
        cv2.waitKey(1)

## End of recognition code
    

#tkinter GUI code below
root = Tk()
root.title("BRAINCASE")

#GUI Frames
mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

title_frame = ttk.Frame(mainframe, padding="3 3 12 12")
title_frame.grid(column=0,row=0, padx=5, pady=5)

wifi_frame = ttk.Labelframe(mainframe, padding="3 3 12 12", borderwidth=2, relief="groove", text="Wi-Fi settings")
wifi_frame.grid(column=0, row=1, padx=5, pady=5)
wifi_frame.grid_remove()

mouse1_frame = ttk.Labelframe(mainframe, padding="3 3 12 12", borderwidth=2, relief="groove", text="Mouse 1")
mouse1_frame.grid(column=1, row=1)

mouse2_frame = ttk.Labelframe(mainframe, padding="3 3 12 12", borderwidth=2, relief="groove", text="Mouse 2")
mouse2_frame.grid(column=2, row=1)

mouse3_frame = ttk.Labelframe(mainframe, padding="3 3 12 12", borderwidth=2, relief="groove", text="Mouse 3")
mouse3_frame.grid(column=1, row=2)

mouse4_frame = ttk.Labelframe(mainframe, padding="3 3 12 12", borderwidth=2, relief="groove", text="Mouse 4")
mouse4_frame.grid(column=2, row=2)

mouse_frame = [None, mouse1_frame, mouse2_frame, mouse3_frame, mouse4_frame]
for frame in mouse_frame:
    if frame != None:
        frame.grid_configure(sticky=(N, S), padx=5, pady=5)
        frame.grid_remove()

#Font
default_font = font.nametofont("TkDefaultFont")
default_font.configure(size=10)

#tkinter variables (Lists containing groups of variables are defined at the end of
# each block. This allows the code to be formatted better by indexing through
# a list instead of typing the whole name out. The 1st entry is None, so the list
# can start at index 1.)

# Stores IP addresses for each board
board_ip_1 = StringVar()
board_ip_1.set("IP address: " + "Not initialized")
board_ip_2 = StringVar()
board_ip_2.set("IP address: " + "Not initialized")
board_ip_3 = StringVar()
board_ip_3.set("IP address: " + "Not initialized")
board_ip_4 = StringVar()
board_ip_4.set("IP address: " + "Not initialized")
board_ip = [None, board_ip_1, board_ip_2, board_ip_3, board_ip_4]

# Variables for test LED radio buttons
led_test_var_1 = StringVar()
led_test_var_1.set("OFF")
led_test_var_2 = StringVar()
led_test_var_2.set("OFF")
led_test_var_3 = StringVar()
led_test_var_3.set("OFF")
led_test_var_4 = StringVar()
led_test_var_4.set("OFF")
led_test_var = [None, led_test_var_1, led_test_var_2, led_test_var_3, led_test_var_4]

# Stores battery percentages for each board
battery_1 = StringVar()
battery_1.set("Battery: " + "Not initialized")
battery_2 = StringVar()
battery_2.set("Battery: " + "Not initialized")
battery_3 = StringVar()
battery_3.set("Battery: " + "Not initialized")
battery_4 = StringVar()
battery_4.set("Battery: " + "Not initialized")
battery = [None, battery_1, battery_2, battery_3, battery_4]

# Variables used in the Wi-Fi frame
wifi_network = StringVar()
wifi_password = StringVar()
 
# Enables password text field if 1
password_enable = StringVar()
password_enable.set("0")

wifi_confirmed = BooleanVar()
wifi_confirmed.set(False)

total_mice = IntVar()
total_mice.set(1)

# Stores if "Ready" checkbox is checked for each board
mouse_ready_1 = IntVar()
mouse_ready_1.set(0)
mouse_ready_2 = IntVar()
mouse_ready_2.set(0)
mouse_ready_3 = IntVar()
mouse_ready_3.set(0)
mouse_ready_4 = IntVar()
mouse_ready_4.set(0)
mouse_ready = [None, mouse_ready_1, mouse_ready_2, mouse_ready_3, mouse_ready_4]

#Title frame widgets
ttk.Label(title_frame, text="BRAINCASE", font=("Impact", 50)).grid(column=0, row=0, columnspan=2, sticky=(N, W, E ,S))
ttk.Label(title_frame, text="Select number of mice:").grid(column=0, row=1)
total_mice_menu = OptionMenu(title_frame, total_mice, 1, 2, 3, 4)
total_mice_menu.grid(column=1, row=1)
total_mice_confirm = ttk.Button(title_frame, text="Confirm", command=total_mice_confirmer)
total_mice_confirm.grid(column=0, row=2, padx=10, pady=5, sticky=(W, E))
total_mice_edit = ttk.Button(title_frame, text="Edit", command=total_mice_editor, state="disabled")
total_mice_edit.grid(column=1, row=2, padx=10, pady=5, sticky=(W, E))
title_frame.columnconfigure(0, weight=1)
title_frame.columnconfigure(1, weight=1)

st = ttk.Style()
st.configure('start.TButton', font=('TkDefaultFont', 25, 'bold'), foreground='green')
start_button = ttk.Button(title_frame, text="Start tracking", state="normal", style='start.TButton', command=recognition_code)
start_button.grid(column=0, row=3, columnspan=2, pady=5)
start_button.grid_remove()

read_battery_button = ttk.Button(title_frame, text="Refresh battery percentages", state="disable", command=read_battery)
read_battery_button.grid(column=0, row=4, columnspan=2, pady=5)
read_battery_button.grid_remove()

#Wi-Fi settings frame widgets
ttk.Label(wifi_frame, text="Network name:").grid(column=0, row=0, sticky=(W, E), pady=5)
network_field = ttk.Entry(wifi_frame, textvariable=wifi_network)
network_field.grid(column=1, row=0, sticky=(W, E), pady=5)
ttk.Label(wifi_frame, text="Enter password?").grid(column=0, row=1, sticky=(W, E), pady=5)
password_enable_box = ttk.Checkbutton(wifi_frame, text="Yes", variable=password_enable, command=password_enabler)
password_enable_box.grid(column=1, row=1, sticky=(W, E), padx=10, pady=5)
ttk.Label(wifi_frame, text="Password:").grid(column=0, row=2, sticky=(W, E), pady=5)
password_field = ttk.Entry(wifi_frame, textvariable=wifi_password, show="*", state="disabled")
password_field.grid(column=1, row=2, sticky=(W, E), pady=5)
wifi_confirm_button = ttk.Button(wifi_frame, text="Confirm", command=wifi_confirmer)
wifi_confirm_button.grid(column=0, row=3, columnspan=2, pady=5)
wifi_edit_button = ttk.Button(wifi_frame, text="Edit", command=wifi_editor, state="disabled")
wifi_edit_button.grid(column=0, row=4, columnspan=2, pady=5)

#Mouse frame widget objects and lists
mouse1_wifi_button = ttk.Button()
mouse2_wifi_button = ttk.Button()
mouse3_wifi_button = ttk.Button()
mouse4_wifi_button = ttk.Button()
mouse_wifi_button = [None, mouse1_wifi_button, mouse2_wifi_button, mouse3_wifi_button, mouse4_wifi_button]

mouse1_getip_button = ttk.Button()
mouse2_getip_button = ttk.Button()
mouse3_getip_button = ttk.Button()
mouse4_getip_button = ttk.Button()
mouse_getip_button = [None, mouse1_getip_button, mouse2_getip_button, mouse3_getip_button, mouse4_getip_button]

mouse1_resetip_button = ttk.Button()
mouse2_resetip_button = ttk.Button()
mouse3_resetip_button = ttk.Button()
mouse4_resetip_button = ttk.Button()
mouse_resetip_button = [None, mouse1_resetip_button, mouse2_resetip_button, mouse3_resetip_button, mouse4_resetip_button]

led_test_on_1 = ttk.Radiobutton()
led_test_on_2 = ttk.Radiobutton()
led_test_on_3 = ttk.Radiobutton()
led_test_on_4 = ttk.Radiobutton()
led_test_on = [None, led_test_on_1, led_test_on_2, led_test_on_3, led_test_on_4]

led_test_off_1 = ttk.Radiobutton()
led_test_off_2 = ttk.Radiobutton()
led_test_off_3 = ttk.Radiobutton()
led_test_off_4 = ttk.Radiobutton()
led_test_off = [None, led_test_off_1, led_test_off_2, led_test_off_3, led_test_off_4]

ready_box_1 = ttk.Checkbutton()
ready_box_2 = ttk.Checkbutton()
ready_box_3 = ttk.Checkbutton()
ready_box_4 = ttk.Checkbutton()
ready_box = [None, ready_box_1, ready_box_2, ready_box_3, ready_box_4]

#Mouse frame widget setup
for i in range(1, 5):
    ttk.Label(mouse_frame[i], textvariable=battery[i], anchor="center").grid(column=0, row=0, columnspan=2, sticky=(W, E)) #Battery display
    ttk.Label(mouse_frame[i], textvariable=board_ip[i], anchor="center").grid(column=0, row=1, sticky=(W, E), padx=10) #IP address display
    mouse_wifi_button[i] = ttk.Button(mouse_frame[i], text="Connect to Wi-Fi", command=partial(wifi_connector, i), state="disabled")
    mouse_wifi_button[i].grid(column=0, row=2, sticky=(W, E), padx=10, pady=5)
    mouse_getip_button[i] = ttk.Button(mouse_frame[i], text="Get IP address", command=partial(get_ip, i))
    mouse_getip_button[i].grid(column=0, row=3, sticky=(W, E), padx=10, pady=5)
    mouse_resetip_button[i] = ttk.Button(mouse_frame[i], text="Reset IP address", command=partial(reset_ip, i))
    mouse_resetip_button[i].grid(column=0, row=4, sticky=(W, E), padx=10, pady=5)
    
    ttk.Label(mouse_frame[i], text="Connection test", anchor="center").grid(column=1, row=1, sticky=(W, E), padx=10, pady=5)
    led_test_on[i] = ttk.Radiobutton(mouse_frame[i], text="LED ON", variable=led_test_var[i], value="ON", command=partial(turn_led_on, i), state="disabled")
    led_test_on[i].grid(column=1, row=2, padx=10, pady=5)
    led_test_off[i] = ttk.Radiobutton(mouse_frame[i], text="LED OFF", variable=led_test_var[i], value="OFF", command=partial(turn_led_off, i), state="disabled")
    led_test_off[i].grid(column=1, row=3, padx=10, pady=5)
    ready_box[i] = ttk.Checkbutton(mouse_frame[i], text="Ready", variable=mouse_ready[i], command=ready_check, state="disabled")
    ready_box[i].grid(column=1, row=4, padx=10)

root.resizable(0, 0) #Prevent window resizing
root.mainloop() #Start tkinter GUI