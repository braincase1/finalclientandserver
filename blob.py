#!/usr/bin/python

# Tedy Notes
# Feature Enfineering
# Use it to extract param values for best preformance

# Standard imports
import cv2
import numpy as np
import math
import socket
import io
import struct
import time
import pickle
import zlib
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
from io import BytesIO

import _thread
conf = {}
maxMice = 4
PORT = 8000
port = 3000

on = False

def turn_led_on(mouse_num):
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ip = conf[mouse_num]
    #ip = ip[12:]
    client.connect((ip, port))
    
    client.send("GET /27/on\n\n".encode()) #LED on command
    from_server = client.recv(4096)
    
    client.close()
    
    print(from_server.decode())

def turn_led_off(mouse_num):
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ip = conf[mouse_num]
    #ip = ip[12:]
    client.connect((ip, port))
    
    client.send("GET /27/off\n\n".encode()) #LED off command
    from_server = client.recv(4096)

    client.close()
    
    print(from_server.decode())

def calculateDistance(x1,y1,x2,y2):  
     dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)  
     return dist  

miceDict = {}

def circle(x1, y1, r1, x2, y2, r2): 
   
    distSq = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);  
    radSumSq = (r1 + r2) * (r1 + r2);  
    if (distSq == radSumSq): 
        return 1 
    elif (distSq > radSumSq): 
        return -1 
    else: 
        return 0 

def trackMouseCordinates(keypoints):
  # print(len(keypoints))
  if(len(keypoints) == maxMice):
    index = 0
    for point in keypoints:
      miceDict['m'+ str(index)] = {
        'x': point.pt[0],
        'y': point.pt[1],
        'r': point.size/2
      }
      index += 1

    # print(miceDict)
    index = 0
    while(index < maxMice):
      base = miceDict.get('m'+ str(index))
      for i in range(index, maxMice):
        if(i != index):
          compare = miceDict.get('m' + str(i))
          t = circle(base.get('x'), base.get('y'), base.get('r'), compare.get('x'), compare.get('y'), compare.get('r'))
          if (t >= 0): 
            print('m'+str(i)+' touches m'+str(index))
            # turn_led_on(i)
            # turn_led_on(index)
            # time.sleep(1000)
            # turn_led_off(i)
            # turn_led_off(index)
            
      index += 1
  # else:
  #   differences = {}
  #   for key, value in miceDict.items():
  #     differences[key] = 10000000000
  #     for keypoint in keypoints:
  #       difference = abs(calculateDistance(value[0],keypoint.pt[0],value[1],keypoint.pt[1]))
  #       if(differences[key] > difference):
  #         differences[key] = difference
  #     # print(differences[key])
encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

   

def sendData():
  img_counter = 0
  # cap = cv2.VideoCapture('./Test_C_1.avi')
  cap = cv2.VideoCapture(0)


  # Setup SimpleBlobDetector parameters.
  params = cv2.SimpleBlobDetector_Params()

  # Change thresholds
  params.minThreshold = 1
  params.maxThreshold = 200

  # Filter by Color
  params.filterByColor = True
  params.blobColor = 0

  # Filter by Area.
  params.filterByArea = True
  params.minArea = 1500

  # Filter by Circularity
  params.filterByCircularity = False
  params.minCircularity = 0.01

  # Filter by Convexity
  params.filterByConvexity = False
  params.minConvexity = 0.01

  # Filter by Inertia
  params.filterByInertia = False
  params.minInertiaRatio = 0.01

  # Create a detector with the parameters
  ver = (cv2.__version__).split('.')
  if int(ver[0]) < 3:
    detector = cv2.SimpleBlobDetector(params)
  else:
    detector = cv2.SimpleBlobDetector_create(params)
  client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  client_socket.connect(('localhost', 8485))
  connection = client_socket.makefile('wb')
  while True:
    ret, im = cap.read()
    # Detect blobs.
    keypoints = detector.detect(im)
    trackMouseCordinates(keypoints)
  # Draw detected blobs as red circles.
  # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures
  # the size of the circle corresponds to the size of blob

    im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array(
        []), (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
  # Show blobs
  #   cv2.imshow("Keypoints", im_with_keypoints)
    # cv2.imshow('object detection', cv2.resize(im_with_keypoints, (800, 600)))
    result, frame = cv2.imencode('.jpg', im_with_keypoints, encode_param)
  #    data = zlib.compress(pickle.dumps(frame, 0))
    data = pickle.dumps(frame, 0)
    size = len(data)


    # print("{}: {}".format(img_counter, size))
    client_socket.sendall(struct.pack(">L", size) + data)
    img_counter += 1

    if cv2.waitKey(1) & 0xFF == ord('q'):
      out = cv2.imwrite('capture.jpg', im_with_keypoints)
      cap.release()
      cv2.destroyAllWindows()
      break

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'Hello, world!')

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        self.send_response(200)
        self.end_headers()
        b = json.loads(body)
        maxMice = int(b.get('maxMice'))
        del b['maxMice']
        conf = b
        response = BytesIO()
        response.write(b'This is POST request. ')
        response.write(b'Received: ')
        response.write(body)
        print('conf is',conf)
        print('maxMice is', maxMice)
        self.wfile.write(response.getvalue())
        print('sending data')
        _thread.start_new_thread(sendData, ())

httpd = HTTPServer(('localhost', 8000), SimpleHTTPRequestHandler)

httpd.serve_forever()